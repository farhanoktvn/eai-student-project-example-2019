package com.adf.tugasakhir.service;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import com.adf.tugasakhir.model.Conference;
import com.adf.tugasakhir.repository.ConferenceRepo;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ConferenceServiceImpl
 */
@Log4j2
@Service
public class ConferenceServiceImpl implements ConferenceService {

    @Autowired
    ConferenceRepo conferenceRepo;

    @Override
    public List<Conference> getAllLatestConference() {
        return conferenceRepo.findAllByOrderByIdAsc();
    }

    @Override
    public void addConference(Conference conference) {
        log.info("Created conference");
        conferenceRepo.save(conference);
    }

    @Override
    public void removeConferenceById(Long id) {
        conferenceRepo.deleteById(id);
    }

    @Override
    public Conference getConferenceById(Long id) throws EntityNotFoundException {
        if (!conferenceRepo.existsById(id)) {
            throw new EntityNotFoundException();
        }
        return conferenceRepo.getOne(id);
    }

    @Override
    public void updateConference(Conference conference) throws EntityNotFoundException {
        if (!conferenceRepo.existsById(conference.getId())) {
            throw new EntityNotFoundException();
        }
        conferenceRepo.save(conference);
    }
}