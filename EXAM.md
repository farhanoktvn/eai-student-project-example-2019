# CSCM602023 Advanced Programming (KKI) - 2020 - Final Programming Exam

> Muhammad Farhan Oktavian - 1806173512

### Task 1: Ensuring 12 Factors (mandatory)
##### 1. Codebase  
In this final exam, all code of the application is already in a single codebase. There is not any issue regarding this implementation.
##### 2. Dependencies
Since we use Gradle to build the Spring app, all the dependencies are included in the build.gradle file, ensuring all the dependencies installed in all machine where the app run.
##### 3. Config 
There is a mistake that I found in the code. The api key on application.properties should be put on environment variable so it is hidden from the codebase.
##### 4. Backing services
The database used by the application (PostgreSQL) is already separated from the app and can be attached/detached anytime by JDBC.
##### 5. Build, release, run
In this case, we execute those stages when we push the code to GitLab. Since we make a gitlab-ci configuration, the process run automatically whenever new code are pushed. Also, Heroku already applied this process since it pull the code from git, build, release, and then run the application.
##### 6. Processes
All data that should be saved are immediately saved onto the database, in this case PostgreSQL.
##### 7. Port binding
In local development, the application runs on port 8080 and can be configured for use with container. Since we use Spring, we just have to specify which port to use in application.properties or Dockerfile when using container.
##### 8. Concurrency
Since Spring create a bean for every repository/service, it is applying concurrency for it processes.
##### 9. Disposability
This feature is already implemented by Spring with Gradle. For running the application we just have to start the jar that is build after release. Furthermore, when there is new deployment, the current app can be shutdown and the new one will be run after build and release.
##### 10. Dev/prod parity
To minimize the parity between development and production, I interact with the same type of database in the local and Heroku (PostgreSQL). I also try to use Linux as possible since the gitlab-ci and heroku are also based on Linux. Since we have deadline, changes are often deployed with around one deployment every hour.
##### 11. Logs
System.err on ScienceDirectPaperFinder are changed to log. Log.info are also added when creating conference and paper.
##### 12. Admin processes
Admin processes can be executed through Gradle commands such as run, check, and build.

### Task 2: Measuring Number of Method Invocation (mandatory)
To measure the amount of invokes for the method we need to add Springboot actuator so Prometheus can find our endpoint and Micrometer Prometheus to actually measure each invoke. Then we need to create registry config for the @Timed. Lastly, we need to change WebSecurityConfig so Prometheus can see our endpoint withou logging. The data can now be seen by query extract_key_phrases_seconds_count.
![Prometheus Demo](exam-img/prometheus.JPG)

### Task 3: Refactor classes in the Model Layer (mandatory)
To refactor the classes in model layer, we use Lombok with its annotation. There is only one class in Model layer which is Conference. Taking a brief look at the class, we can omit its setters/getters and add @Setter and @Getter annotation to the class. It also have an empty constructor, so we can add @NoArgsConstructor.

### Task 4: Implement Data Persistence for ```Paper``` Class (mandatory)
In order to save Paper class to the database, we need to change its type, add @Entity and add id variable. Then, we can also refactor the code using Lombok annotation processor.

### Task 5: Improve Code Quality (optional)

### Work Checklist
- [x] Fork the project to your own GitLab project namespace

- [x] Setup new Heroku app and configure CI/CD variables in your own fork

- [x] Clone the fork codebase to your local machine

- [x] Begin working on on the mandatory tasks

- [x] Do not forget to push latest state of your work to the online Git repository

- [x] Submit the following on Scele:
  - URL to your GitLab project (make sure it is public!)